<?php 
if( ! defined('BASEPATH') ) exit('No direct script access allowed');

$config = array(

      'cliente_put' => array(
            array( 'field'=>'no_cliente', 'label'=>'no_cliente','rules'=>'trim|required|integer' ),
            array( 'field'=>'nombre', 'label'=>'nombre','rules'=>'trim|required|regex_match[/^([a-z ñáéíóú0-9&])([A-Z ÑÁÉÍÓÚ0-9&])+$/i]' ),
            array( 'field'=>'tramite', 'label'=>'tramite','rules'=>'trim|required|alpha_numeric' ),
            array( 'field'=>'tipo', 'label'=>'tipo','rules'=>'trim|required|alpha' ),
            array( 'field'=>'marca', 'label'=>'marca','rules'=>'trim|required|alpha_numeric_spaces' ),
            array( 'field'=>'modelo', 'label'=>'modelo','rules'=>'trim|required|alpha_numeric_spaces' ),
            array( 'field'=>'version', 'label'=>'version','rules'=>'trim|required|alpha_numeric_spaces' ),
            array( 'field'=>'anio', 'label'=>'anio','rules'=>'trim|required|integer|exact_length[4]' ),
            array( 'field'=>'precio', 'label'=>'precio','rules'=>'trim|required|numeric' ),
            array( 'field'=>'edo_origen', 'label'=>'edo_origen','rules'=>'trim|required|alpha' ),
            array( 'field'=>'pdv', 'label'=>'pdv','rules'=>'trim|required|regex_match[/^([a-z ])+$/i]' ),
            array( 'field'=>'vendedor', 'label'=>'vendedor','rules'=>'trim|required|regex_match[/^([a-z ])+$/i]' ),
            array( 'field'=>'ejecutivo', 'label'=>'ejecutivo','rules'=>'trim|required|regex_match[/^([a-z ])+$/i]' ),
            array( 'field'=>'edo_cotizacion1', 'label'=>'edo_cotizacion1','rules'=>'trim|required|alpha|exact_length[3]' )      
      ),
      'cotizacion_post' => array(
            array( 'field'=>'folio', 'folio'=>'id','rules'=>'trim|required|integer' ),
            array( 'field'=>'correo', 'label'=>'usuario','rules'=>'trim|required|valid_email' ),
            array( 'field'=>'edo_emplacamiento', 'label'=>'edo_emplacamiento','rules'=>'trim|required|regex_match[/^([a-z ])+$/i]' ),
            array( 'field'=>'id', 'label'=>'id','rules'=>'trim|required|integer' )
      ),
      'prueba_put' => array(
            array( 'field'=>'no_cliente', 'label'=>'no_cliente','rules'=>'trim|required|integer' ),
            array( 'field'=>'nombre', 'label'=>'nombre','rules'=>'trim|required|regex_match[/^([a-z ])+$/i]' ),
            array( 'field'=>'edo_cotizacion1', 'label'=>'edo_cotizacion1','rules'=>'trim|required|alpha|exact_length[3]' )  
      ),

);

?>