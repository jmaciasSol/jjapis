<?php

  defined('BASEPATH') OR exit('No direct script access allowed');

  require APPPATH.'/libraries/REST_Controller.php';

  require 'composer/vendor/autoload.php';
  
  use Aws\S3\S3Client;
  use Aws\Exception\AwsException;
 
  class Cotizaciones extends REST_Controller {

    public function __construct(){
      // Llamado del constructor del padre
      parent::__construct();
      $this->load->database();
      $this->load->model('Cliente_model');
      $this->load->helper('utilidades');

      }

    public  function encrypt_decrypt($action, $string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        $secret_key = '1234567812345678';
        $secret_iv = '1234567812345678';

        // hash
        $key = hash('sha256', $secret_key);
        
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ( $action == 'encrypt' ) {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if( $action == 'decrypt' ) {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
      }



      public function index_put(){

        $email = $this->input->get_request_header("USER");
        $data = $this->put();

        $this->load->library('form_validation');
        $this->form_validation->set_data($data);


        if(!isset($data['edo_cotizacion2'])){
           $cotizacion2 = 'NA';
        }else{
          $cotizacion2 = $data['edo_cotizacion2'];
        }

        if(!isset($data['edo_cotizacion3'])){
          $cotizacion3 = 'NA';
        }
        else{
          $cotizacion3 = $data['edo_cotizacion3'];
        }

        if(!isset($data['fecha_factura'])){
          $fecha_factura = '';
        }
        else{
          $fecha_factura = $data['fecha_factura'];
          $data_date = explode("/", $fecha_factura);

          $fecha_factura = $data_date[2].$data_date[1].$data_date[0];
        }

        if(!isset($data['comentarios'])){
          $comentarios = '';
        }else{
          $comentarios = $data['comentarios'];
        }

        if( $this->form_validation->run('cliente_put')){
    
          $registro = array(
            'NO_CLIENTE' => htmlspecialchars($data['no_cliente']),
            'NOMBRE' => encrypt_decrypt('encrypt', $data['nombre']),
            'TRAMITE' => $data['tramite'],
            'TIPO' => $data['tipo'],
            'MARCA' => $data['marca'],
            'MODELO' => $data['modelo'],
            'VERSION' => $data['version'],
            'ANIO' => $data['anio'],
            'VALOR_FACTURA' => $data['precio'],
            'EDO_ORIGEN' => $data['edo_origen'],
            'PDV' => $data['pdv'],
            'VENDEDOR' => $data['vendedor'],
            'EJECUTIVO' => $data['ejecutivo'],
            'EDO_COTIZACION_1' => $data['edo_cotizacion1'],
            'EDO_COTIZACION_2' => $cotizacion2,
            'EDO_COTIZACION_3' => $cotizacion3,
            'USUARIO' => $email,
            'FECHA_FACTURA' => $fecha_factura,
            'COMENTARIOS' => $comentarios
          );

          //Se crea envento en staging 

          $this->db->insert('tbl_staging', $registro);
         
          
          if($this->db->affected_rows() > 0){

            $last_id = $this->db->insert_id();

            // WHEN S.TRAMITE > 1 THEN S.HJID_CONTRATOS
            $sql = "SELECT 
              CASE 
              WHEN S.TRAMITE = 1 THEN MAX(C.ID)
              WHEN S.TRAMITE > 1 THEN MAX(C.ID)
              END HJID_CONTRATOS
              FROM tbl_contratos C
              INNER JOIN tbl_staging S ON C.NO_CLIENTE = S.NO_CLIENTE
              WHERE S.NO_CLIENTE = ? LIMIT 1";
            
            $query = $this->db->query($sql, $data['no_cliente']);
            
            $id = $query->result_array();

            
            $sql2 = "SELECT * FROM v_cotizaciones v WHERE v.ID = ? ORDER BY FOLIO";
            $query2 = $this->db->query($sql2,array($id[0]['HJID_CONTRATOS']));
       
            $respuesta = array();

            array_push($respuesta, array('id' => $id[0]['HJID_CONTRATOS'] ));

            $cot = array();

            $result = $query2->result_array();

            foreach ($result as $cotizacion => $cot ) {
              array_pop($cot);
              array_push($respuesta, array('cotizacion' => $cot));
            }

            $r = array(
              'value' => $respuesta,
            );

           $this->response($r);

        
          }else{
            $respuesta = array(
            'err' => TRUE,
            'mensanje' => 'El Registro no pudo ser insertado en la base'
            );

            $this->response( $respuesta, REST_Controller::HTTP_BAD_REQUEST );
           
          }

        }else{
          $respuesta = array(
            'err' => TRUE,
            'mensanje' => 'Hay errores en el envio de información',
            'errores' => $this->form_validation->get_errores_arreglo()
          );

          $this->response( $respuesta, REST_Controller::HTTP_BAD_REQUEST );
        }
      
      }

      public function reporte_get(){

        
        $sql = "SELECT * FROM V_REPORT";
        $query = $this->db->query($sql);   

        $result = $query->result_array();
    
        $data = [];

        for($i=0; $i < count($result); $i++){
           $result[$i]['NOMBRE'] = encrypt_decrypt('decrypt', $result[$i]['NOMBRE']);
        }
          
        $this->response($result);
      }

      public function busqueda_get(){
        
        $no_cliente = $this->get( 'cliente' );
        $id_contratos = $this->get( 'contratos' );
        
        
        if($no_cliente === null){
            $this->response( [
                'status' => false,
                'message' => 'No users were found'
            ], 404 );
        
        }else{

          $sql = "SELECT * FROM db_sistema.v_bitacora B WHERE B.NO_CLIENTE = ?";
          $query = $this->db->query($sql,$no_cliente);
          
          $this->response( $query->result_array(), 200 );

        }
        
        if($id_contratos === null){
          $this->response( [
                'status' => false,
                'message' => 'No users were found'
            ], 404 );
        
        }else{
          $sql = "SELECT * FROM db_sistema.v_bitacora B WHERE B.ID_TBL_CONTRACT = ?";
          $query = $this->db->query($sql,$id_contratos);

          $this->response( $query->result_array(), 200 );
       
        }

      }


      public function uploadimage_post(){

        $data = $this->post();

        $this->load->helper(array('form', 'url'));



        $carpeta = 'uploads';
        if (!file_exists($carpeta)) {
          mkdir($carpeta, 0777, true);
        }

        $path = "uploads/image.png";

        $content = file_put_contents($path,base64_decode($data['contentBytes']));
        
        if($content){
         $code = 200;
        }else{
         $code = 500;
        }

        $this->set_response($code, REST_Controller::HTTP_CREATED);

      }


      public function documentos_post(){

        $data = $this->post();

        $registro = array(
          'FOLIO' => $data['folio'],
          'PATH' => $data['path'],
          'NOMBRE' => $data['nombre'],
          'USUARIO' => $data['usuario']
        );

        $this->db->insert('tbl_bitacora_d', $registro);

        if($this->db->affected_rows() > 0){
          
          $respuesta = array(
            'err' => FALSE,
            'mensanje' => 'La informacion se ha guradado correctamente'
          );

          $this->response( $respuesta, 200 );
      
        }else{
          
          $respuesta = array(
            'err' => TRUE,
            'mensanje' => 'El Registro no pudo ser insertado en la base'
          );

          $this->response( $respuesta, REST_Controller::HTTP_BAD_REQUEST );
         
        }         

      }


      public function metamail_post(){

        $data = $this->post();

        $registro = array(
          'FECHA' => $data['fecha'],
          'EMAIL_ID' => $data['email_id'],
          'FROM_' => $data['from_'],
          'CC_' => $data['cc_'],
          'HAS_ATTACHMENT' => $data['has_attachment'],
          'EMAIL_BODY' => $data['email_body']
        );

        /*
          insert into tbl_emails(FECHA, EMAIL_ID, FROM_, CC_, HAS_ATTACHMENT, EMAIL_BODY)
          values(now(),'15646548484','jonathan.macias@jjsolucionesempresariales.com','jonathan.macias@jjsolucionesempresariales.com',1,'kodffodfodfodfodfodfdfdf')
        */

        $this->db->insert('tbl_emails', $registro);

        if($this->db->affected_rows() > 0){
          
          $respuesta = array(
            'err' => FALSE,
            'mensanje' => 'La informacion se ha guradado correctamente'
          );

          $this->response( $respuesta, 200 );
      
        }else{
          
          $respuesta = array(
            'err' => TRUE,
            'mensanje' => 'El Registro no pudo ser insertado en la base'
          );

          $this->response( $respuesta, REST_Controller::HTTP_BAD_REQUEST );
         
        }         

      }
    
  }

